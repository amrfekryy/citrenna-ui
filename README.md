Instructions for package maintainers

#### Developing
- all components must be written inside `./src` and exported from `./src/index.ts`

#### Testing Locally
While working on a component, you can test it locally before publishing:
- create a new react app for testing: `npx create-react-app package-testing`
- link the package locally to the testing app:
  - `cd citrenna-ui` then `npm link`
  - `cd package-testing` then `npm link citrenna-ui`
- test a component:
  - add your component in `citrenna-ui/src` and export it from `citrenna-ui/src/index.ts`   
  - `cd citrenna-ui` then `npm run build` for one build, or `npm run watch` for auto build
  - import and test the component in the testing app

#### Publishing
You need to be added as a package maintainer to be able to publish new chages
- upgrade package `version` in `package.json`
- add package changes: `cd citrenna-ui` then `npm run build`
- publish changes: `npm publish`
